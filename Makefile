CC     ?= cc
CFLAGS ?= -O2
CFLAGS += -std=c2x -Wall -Wextra -pedantic

# Command line program to view PNG images.
VIEW   = open

.PHONY: run clean generate
run: mkgraph
	./mkgraph > graph.gv
	dot graph.gv -Tpng -o graph.png
	$(VIEW) graph.png

clean:
	rm -f mkgraph graph.gv graph.png

mkgraph: mkgraph.c
