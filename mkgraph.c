#include <stdbool.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

enum edge_type {
	EDGE_NONE,
	EDGE_HALL,
	EDGE_DOOR,
	EDGE_SECRET,
	EDGE_ONE_WAY,
	EDGE_MAX
};

const uint32_t edge_type_odds[] = {
	0,
	55,  // 55% are just hallways
	95,  // 40% have doors
	99,  // 4% are secret
	100, // 1% are one-way
	100
};

const char* const edge_type_names[] = {
	"invalid",
	"   ",
	" D ",
	" S ",
	"   ",
	"invalid"
};

// Return a (weighted) random edge type.
enum edge_type
random_edge_type(void)
{
	uint32_t roll = arc4random_uniform(edge_type_odds[EDGE_MAX]) + 1;
	for (size_t i = 0; i < EDGE_MAX; i++) {
		if (roll <= edge_type_odds[i]) {
			return (enum edge_type) i;
		}
	}
	return EDGE_MAX;
}

enum node {
	NODE_SUN,
	NODE_MON,
	NODE_TUE,
	NODE_WED,
	NODE_THU,
	NODE_FRI,
	NODE_SAT,
	NODE_MAX
};

const char* const node_names[] = {
	"Sunday",
	"Monday",
	"Tuesday",
	"Wednesday",
	"Thursday",
	"Friday",
	"Saturday",
	"invalid"
};

// Return a random node from the graph.
enum node
random_node(void)
{
	return arc4random_uniform(NODE_MAX);
}


// Keep track of visited nodes, for various algorithms.
bool node_visited[NODE_MAX] = { 0 };
size_t num_visited = 0;

// Mark a node as visited.
void
visit_node(enum node n)
{
	node_visited[n] = true;
	num_visited++;
}

// Mark all nodes as unvisited.
void
reset_nodes(void)
{
	for (size_t i = 0; i < NODE_MAX; i++) {
		node_visited[i] = false;
	}
	num_visited = 0;
}

// Return a random node that hasn't been visited, or NODE_MAX.
enum node
random_unvisited_node(void)
{
	if (num_visited == NODE_MAX) {
		return NODE_MAX;
	}

	enum node n;
	do {
		n = random_node();
	} while (node_visited[n]);

	return n;
}

// Return a random node other than the one provided.
enum node
random_other_node(enum node n)
{
	enum node o;
	do {
		o = random_node();
	} while (o == n);

	return o;
}

// Keep track of which nodes are area exits.
bool node_is_exit[NODE_MAX] = { 0 };

// Mark some number of nodes as exits.
void
add_exits(uint32_t count)
{
	count = count > NODE_MAX ? NODE_MAX : count;

	reset_nodes();
	for (size_t i = 0; i < count; i++) {
		enum node n = random_unvisited_node();
		node_is_exit[n] = true;
		visit_node(n);
	}
}

enum content_type {
	CONTENT_EMPTY,
	CONTENT_MONSTER,
	CONTENT_SPECIAL,
	CONTENT_TRAP,
	CONTENT_MAX
};

const char* const content_type_names[] = {
	" ",
	"Monster",
	"Special",
	"Trap",
	"invalid"
};

const char* const content_type_colors[] = {
	"#ffffff",
	"#005a5f",
	"#5317ac",
	"#721045",
	"#ff00ff"
};

const uint32_t content_type_odds[] = {
	1, // 2-in-6 chance of empty
	3, // 2-in-6 chance of monster
	4, // 1-in-6 chance of special
	5, // 1-in-6 chance of trap
	6
};

const uint32_t treasure_odds[] = {
	1, // 1-in-6 chance in empty room
	3, // 3-in-6 chance in monster room
	0, // 0-in-6 chance in special room
	2, // 2-in-6 chance in trap room
	0
};

// Get a (weighted) random type of content.
enum content_type
random_content_type(void)
{
	uint32_t roll = arc4random_uniform(content_type_odds[CONTENT_MAX]);
	for (size_t i = 0; i < EDGE_MAX; i++) {
		if (roll <= content_type_odds[i]) {
			return (enum content_type) i;
		}
	}
	return CONTENT_MAX;
}

// Return true of a node should have treasure, based on its content type.
bool
node_has_treasure(enum content_type ct)
{
	return arc4random_uniform(6) < treasure_odds[ct];
}

// An adjacency matrix for keeping track of edges. Each cell is an edge
// type.  We're creating an undirected graph, so we take care not to add
// duplicate edges in the `span` and `fill` functions.
enum edge_type matrix[NODE_MAX][NODE_MAX] = { 0 };

// Aldous-Broder algorithm, which is fine for 7 nodes but inefficient for
// larger graphs because it has to cover the entire graph with a random walk.
void
span(void)
{
	reset_nodes();

	enum node current = random_node();
	visit_node(current);

	while (num_visited < NODE_MAX) {
		enum node neighbor = random_other_node(current);
		if (!node_visited[neighbor]) {
			matrix[current][neighbor] = random_edge_type();
			visit_node(neighbor);
		}
		current = neighbor;
	}
}

// Add additional random edges to the graph to achieve the desired density.
void
fill(uint32_t density, uint32_t max_failures) {
	// There are 7 * 7 = 49 edges in the adjacency matrix, but we don't
	// allow self connections, so take away 7. Then divide by two, because
	// we're creating an undirected graph.
	uint32_t max_new_edges = (NODE_MAX * NODE_MAX - NODE_MAX) / 2;

	// Scale the density parameter to get the number of new edges.
	uint32_t new_edges = (density * max_new_edges) / 100;

	// The method we're using is non-deterministic, so we may need to
	// bail out if we can't find a place to create a new edge.
	uint32_t failures = 0;

	while (new_edges != 0 && failures < max_failures) {
		enum node n1 = random_node();
		enum node n2 = random_other_node(n1);

		// Check both pairs in the adjacency matrix, so we don't
		// accidently create a duplicate edge.
		if (matrix[n1][n2] == EDGE_NONE
		    && matrix[n2][n1] == EDGE_NONE) {
			matrix[n1][n2] = random_edge_type();
			new_edges--;
		} else {
			failures++;
		}
	}
}

// Emit Graphvis statements for each node.
void
emit_nodes()
{
	printf("  // Rooms and their contents.\n");
	for (size_t i = 0; i < NODE_MAX; i++) {
		bool is_exit = node_is_exit[i];
		enum content_type ct = random_content_type();
		bool has_treasure = node_has_treasure(ct);

		printf("  %s [label=\"\\N%s\\n\\n%s%s\"",
		       node_names[i],
		       is_exit ? " (E)" : "",
		       content_type_names[ct],
		       has_treasure ? " (T)" : "");
		printf(" fillcolor=\"%s\" fontcolor=\"%s\"]\n",
		       content_type_colors[ct],
		       ct == CONTENT_EMPTY ? "#000000" : "#ffffff");
	}
}

// Emit Graphvis statements for each edge.
void
emit_edges(void)
{
	printf("  // Connections between rooms.\n");
	for (size_t i = 0; i < NODE_MAX; i++) {
		for (size_t j = 0; j < NODE_MAX; j++) {
			enum edge_type et = matrix[i][j];
			if (et == EDGE_NONE) {
				continue;
			}
			printf("  %s -- %s [label=\"%s\",dir=%s];\n",
			       node_names[i],
			       node_names[j],
			       edge_type_names[et],
			       et == EDGE_ONE_WAY ? "forward" : "none");

		}
	}
}

// Emit Graphviz configuration statements.
// TODO: Set these with variables or #defines.
void
emit_config(void)
{
	printf("  // Configuration.\n");
	printf("  layout=\"dot\"\n");
	printf("  minlen=\"1.5\"\n");
	printf("  pad=\"0.25\"\n");
	printf("  ratio=\"1\"\n");
	printf("  size=\"8!\"\n");
	printf("  center=true\n");
	printf("  node [shape=box style=filled width=1.25"
	       " fontname=\"IBMPlexMono-16:SemiBold\"]\n");
	printf("  edge [fontname=\"IBMPlexMono-16:Bold\"]\n");
}

int
main(void)
{
	printf("graph {\n");

	emit_config();

	uint32_t area_exits = arc4random_uniform(4) + 1;
	add_exits(area_exits);
	emit_nodes();
	printf("  // area exits: %u\n", area_exits);

	uint32_t density = arc4random_uniform(50);
	span();
	fill(density, 40);
	emit_edges();
	printf("  // edge density: %u%%\n", density);

	printf("}\n");

	return 0;
}
