# Dungeon23 Tools

Some tools I'm using to help with my attempt at a [#dungeon23](https://seanmccoy.substack.com/p/dungeon23) project.

## mkgraph

Spits out some [Graphviz](https://graphviz.org/) code for a seven-room dungeon graph.  Each week I'll hand-draw a new dungeon area based on a graph produced by this tool.

The default target in the `Makefile` builds the program, runs it to generate a `graph.gv` file, and then passes that  to `dot` to render a `graph.png` image, and then opens it in an image viewer.

You may need to change the `VIEW` variable to something that can open PNG images on your system. Right now it's set to Apple's `open` command.

Here's some example output:

![An example dungeon graph](img/example-graph.png)

And a map that I drew  based on that output:

![An example dungeon map](img/example-drawing.jpg)

### Requirements

The program is written in C, and should compile with recent-ish versions of Clang or GCC, or anything else that supports C99 or newer. I've only built it on macOS, so it may require a bit of modification to work on Linux, and more modification to work on Windows.

It doesn't use any extra libraries, but it does use the non-standard [`arc4random_uniform`](https://man.openbsd.org/arc4random.3) function from BSD-land that only recently landed in [Glibc 2.36](https://sourceware.org/pipermail/libc-alpha/2022-August/141193.html).

### Rooms

I'm using the [OSE random dungeon stocking table](https://oldschoolessentials.necroticgnome.com/srd/index.php/Designing_a_Dungeon#Random_Room_Stocking) to determine whether each room contains monsters, traps, something "special", or is empty, and also if there is any treasure. The specific contents are what I'll be coming up with every day.

Up to 4 rooms in each area are marked as "exits", which are connections to other areas and levels of the dungeon. I'll decide on the specific nature of these connections as I create each area.

### Connections

These can be just about anything.

- **Unlabeled edges:** indicate a hallway or other passage without any obstacles.
- **"D" edges**:  indicate a door or other obstacle.
- **"S" edges**: indicate that the connection is secret or hidden.
- **Arrow edges**: indicate a one-way connection, allowing travel only in the direction of the arrow.
